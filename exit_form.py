# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_exit.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog_1(object):
    def setupUi(self, Dialog_1):
        if not Dialog_1.objectName():
            Dialog_1.setObjectName(u"Dialog_1")
        Dialog_1.resize(900, 550)
        Dialog_1.setMinimumSize(QSize(900, 550))
        Dialog_1.setMaximumSize(QSize(650, 400))
        font = QFont()
        font.setFamily(u"Times New Roman")
        Dialog_1.setFont(font)
        icon = QIcon()
        icon.addFile(u"../p7/myappico.ico", QSize(), QIcon.Normal, QIcon.Off)
        Dialog_1.setWindowIcon(icon)
        Dialog_1.setAutoFillBackground(False)
        Dialog_1.setStyleSheet(u"background: url('E:/p4/qqq.jpg');\n"
"border-radius: 5px;\n"
"\n"
"background-color: qlineargradient(spread:pad, x1:0.0607735, y1:0.159091, x2:1, y2:1, stop:0.707182 rgba(170, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.pushButton_3 = QPushButton(Dialog_1)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setGeometry(QRect(390, 420, 101, 31))
        self.pushButton_3.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"")
        self.label_6 = QLabel(Dialog_1)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(170, 70, 571, 101))
        self.label_6.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 18pt \"Times New Roman\";\n"
"background: none;")
        self.label_11 = QLabel(Dialog_1)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setGeometry(QRect(70, 210, 751, 61))
        font1 = QFont()
        font1.setFamily(u"Times New Roman")
        font1.setPointSize(13)
        font1.setBold(False)
        font1.setItalic(False)
        font1.setWeight(50)
        self.label_11.setFont(font1)
        self.label_11.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 13pt \"Times New Roman\";\n"
"background: rgb(0, 0, 0);\n"
"border-radius: 12px;\n"
"border: 1px solid #ffffff;\n"
"text-align: center;\n"
"margin-left: auto;\n"
"margin-right: auto;")
        self.label_2 = QLabel(Dialog_1)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(-10, 0, 1100, 700))
        self.label_2.setMinimumSize(QSize(1100, 700))
        self.label_2.setStyleSheet(u"background: rgba(0, 0, 0, 0.3)")
        self.pushButton_4 = QPushButton(Dialog_1)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setGeometry(QRect(300, 330, 291, 41))
        self.pushButton_4.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"")
        self.label_2.raise_()
        self.pushButton_3.raise_()
        self.label_6.raise_()
        self.label_11.raise_()
        self.pushButton_4.raise_()

        self.retranslateUi(Dialog_1)

        QMetaObject.connectSlotsByName(Dialog_1)
    # setupUi

    def retranslateUi(self, Dialog_1):
        Dialog_1.setWindowTitle(QCoreApplication.translate("Dialog_1", u"\u0414\u043d\u0435\u0432\u043d\u0438\u043a \u0441\u0430\u043c\u043e\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438", None))
        self.pushButton_3.setText(QCoreApplication.translate("Dialog_1", u"\u0412\u044b\u0445\u043e\u0434", None))
        self.label_6.setText(QCoreApplication.translate("Dialog_1", u"<html><head/><body><p align=\"center\">\u0413\u043e\u0442\u043e\u0432\u043e! \u0424\u0430\u0439\u043b \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0441\u043e\u0437\u0434\u0430\u043d </p><p align=\"center\">\u0438 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d \u0432 \u043f\u0430\u043f\u043a\u0443: </p></body></html>", None))
        self.label_11.setText(QCoreApplication.translate("Dialog_1", u"<html><head/><body><p align=\"center\"><br/></p></body></html>", None))
        self.label_2.setText("")
        self.pushButton_4.setText(QCoreApplication.translate("Dialog_1", u"\u0421\u043e\u0437\u0434\u0430\u0442\u044c \u0434\u043d\u0435\u0432\u043d\u0438\u043a \u0434\u0440\u0443\u0433\u0443", None))
    # retranslateUi

