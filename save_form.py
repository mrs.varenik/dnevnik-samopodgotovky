# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_save.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog_5(object):
    def setupUi(self, Dialog_5):
        if not Dialog_5.objectName():
            Dialog_5.setObjectName(u"Dialog_5")
        Dialog_5.resize(900, 550)
        Dialog_5.setMinimumSize(QSize(900, 550))
        icon = QIcon()
        icon.addFile(u"myappico.ico", QSize(), QIcon.Normal, QIcon.Off)
        Dialog_5.setWindowIcon(icon)
        Dialog_5.setStyleSheet(u"background: url('E:/p4/qqq.jpg');\n"
"\n"
"\n"
"background-color: qlineargradient(spread:pad, x1:0.0607735, y1:0.159091, x2:1, y2:1, stop:0.707182 rgba(170, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.pushButton = QPushButton(Dialog_5)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(710, 240, 91, 31))
        self.pushButton.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.pushButton_2 = QPushButton(Dialog_5)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(350, 420, 131, 31))
        self.pushButton_2.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.label = QLabel(Dialog_5)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(100, 230, 571, 51))
        self.label.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 12pt \"Times New Roman\";\n"
"background: rgb(0, 0, 0);\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;")
        self.label_6 = QLabel(Dialog_5)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(20, 80, 851, 121))
        self.label_6.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 18pt \"Times New Roman\";\n"
"background: none;")
        self.label_7 = QLabel(Dialog_5)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(0, 0, 1000, 700))
        self.label_7.setMinimumSize(QSize(1000, 700))
        self.label_7.setMaximumSize(QSize(60, 90))
        self.label_7.setStyleSheet(u"background: rgba(0, 0, 0, 0.3)")
        self.pushButton_3 = QPushButton(Dialog_5)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setGeometry(QRect(290, 420, 41, 31))
        self.pushButton_3.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 15pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.label_7.raise_()
        self.pushButton.raise_()
        self.pushButton_2.raise_()
        self.label.raise_()
        self.label_6.raise_()
        self.pushButton_3.raise_()

        self.retranslateUi(Dialog_5)

        QMetaObject.connectSlotsByName(Dialog_5)
    # setupUi

    def retranslateUi(self, Dialog_5):
        Dialog_5.setWindowTitle(QCoreApplication.translate("Dialog_5", u"\u0414\u043d\u0435\u0432\u043d\u0438\u043a \u0441\u0430\u043c\u043e\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog_5", u"\u041e\u0431\u0437\u043e\u0440", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog_5", u"\u0412\u043f\u0435\u0440\u0435\u0434", None))
        self.label.setText("")
        self.label_6.setText(QCoreApplication.translate("Dialog_5", u"<html><head/><body><p align=\"center\">\u041f\u043e\u0447\u0442\u0438 \u0433\u043e\u0442\u043e\u0432\u043e!</p><p align=\"center\">\u041e\u0441\u0442\u0430\u043b\u043e\u0441\u044c \u0442\u043e\u043b\u044c\u043a\u043e \u0432\u044b\u0431\u0440\u0430\u0442\u044c \u043c\u0435\u0441\u0442\u043e \u0440\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u044f \u0444\u0430\u0439\u043b\u0430!</p></body></html>", None))
        self.label_7.setText("")
        self.pushButton_3.setText(QCoreApplication.translate("Dialog_5", u"\u2190", None))
    # retranslateUi

