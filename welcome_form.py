# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_welcome.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog_2(object):
    def setupUi(self, Dialog_2):
        if not Dialog_2.objectName():
            Dialog_2.setObjectName(u"Dialog_2")
        Dialog_2.resize(900, 550)
        Dialog_2.setMinimumSize(QSize(900, 550))
        Dialog_2.setMaximumSize(QSize(650, 400))
        font = QFont()
        font.setFamily(u"Times New Roman")
        Dialog_2.setFont(font)
        icon = QIcon()
        icon.addFile(u"../p7/myappico.ico", QSize(), QIcon.Normal, QIcon.Off)
        Dialog_2.setWindowIcon(icon)
        Dialog_2.setStyleSheet(u"background: url('E:/p4/qqq.jpg');\n"
"\n"
"\n"
"background-color: qlineargradient(spread:pad, x1:0.0607735, y1:0.159091, x2:1, y2:1, stop:0.707182 rgba(170, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.label_7 = QLabel(Dialog_2)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(320, 30, 491, 61))
        self.label_7.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 15pt \"Times New Roman\";\n"
"background: none;")
        self.pushButton_2 = QPushButton(Dialog_2)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(400, 460, 101, 31))
        self.pushButton_2.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;")
        self.label = QLabel(Dialog_2)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(-70, 110, 1031, 300))
        self.label.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 12pt \"Times New Roman\";\n"
"background: none;\n"
"")
        self.label_2 = QLabel(Dialog_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(-10, 0, 1100, 700))
        self.label_2.setMinimumSize(QSize(1100, 700))
        self.label_2.setStyleSheet(u"background: rgba(0, 0, 0, 0.3)")
        self.label_2.raise_()
        self.pushButton_2.raise_()
        self.label_7.raise_()
        self.label.raise_()

        self.retranslateUi(Dialog_2)

        QMetaObject.connectSlotsByName(Dialog_2)
    # setupUi

    def retranslateUi(self, Dialog_2):
        Dialog_2.setWindowTitle(QCoreApplication.translate("Dialog_2", u"\u0414\u043d\u0435\u0432\u043d\u0438\u043a \u0441\u0430\u043c\u043e\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438", None))
        self.label_7.setText(QCoreApplication.translate("Dialog_2", u"\u041f\u0440\u0438\u0432\u0435\u0442, \u0441\u0442\u0443\u0434\u0435\u043d\u0442!", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog_2", u"\u0421\u0442\u0430\u0440\u0442!", None))
        self.label.setText(QCoreApplication.translate("Dialog_2", u"<html><head/><body><p align=\"center\">\u041b\u0440, \u0440\u043a, \u0434\u0437, \u043a\u0443\u0440\u0441\u0430\u0447\u0438... \u0422\u0430\u043a \u043c\u043d\u043e\u0433\u043e \u043d\u0443\u0436\u043d\u043e \u0441\u0434\u0430\u0442\u044c \u0441\u0442\u0443\u0434\u0435\u043d\u0442\u0443 \u0437\u0430 \u043e\u0434\u0438\u043d \u0441\u0435\u043c\u0435\u0441\u0442\u0440 </p><p align=\"center\">\u0438, \u0432 \u0438\u0442\u043e\u0433\u0435, \u043d\u0435 \u043e\u0441\u0442\u0430\u0435\u0442\u0441\u044f \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u0434\u043b\u044f \u0444\u0438\u0437\u043a\u0443\u043b\u044c\u0442\u0443\u0440\u044b, \u0430 \u0431\u0435\u0437 \u0437\u0430\u0447\u0435\u0442\u0430 \u0442\u0430\u043a \u043d\u0435</p><p align=\"center\"> \u0445\u043e\u0447\u0435\u0442\u0441\u044f \u043e\u0441\u0442\u0430\u0442\u044c\u0441\u044f! \u041e\u0441\u043e\u0431\u0435\u043d\u043d\u043e \u0432\u043e \u0432\u0440\u0435\u043c\u044f \u0434\u0438\u0441\u0442\u0430\u043d\u0442\u0430, \u043a\u043e\u0433\u0434\u0430 \u0434\u043e"
                        "\u0441\u0442\u0430\u0442\u043e\u0447\u043d\u043e \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c</p><p align=\"center\">\u043e\u0434\u0438\u043d \u043b\u0438\u0448\u044c \u0434\u043d\u0435\u0432\u043d\u0438\u043a \u0441\u0430\u043c\u043e\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438! \u0414\u0430\u043d\u043d\u043e\u0435 \u043f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u0435 \u043f\u043e\u043c\u043e\u0436\u0435\u0442 </p><p align=\"center\">\u043d\u0435 \u0442\u0440\u0430\u0442\u0438\u0442\u044c \u0432\u0440\u0435\u043c\u044f \u043d\u0430 \u0437\u0430\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0434\u043d\u0435\u0432\u043d\u0438\u043a\u0430, \u0430 \u0441\u0430\u043c\u043e \u0441\u0433\u0435\u043d\u0435\u0440\u0438\u0440\u0443\u0435\u0442 \u0435\u0433\u043e \u0442\u0435\u0431\u0435!</p><p align=\"center\"> \u0410 \u0441\u0430\u043c\u043e\u0435 \u0433\u043b\u0430\u0432\u043d\u043e\u0435: \u0431\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u043e, \u0431\u0435\u0437 \u0440\u0435\u0433"
                        "\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438 \u0438 \u0421\u041c\u0421! </p><p align=\"center\">\u0412\u043f\u0435\u0440\u0435\u0434 \u043a \u0437\u0430\u0447\u0435\u0442\u0443 \u043f\u043e \u0444\u0438\u0437\u0440\u0435!</p></body></html>", None))
        self.label_2.setText("")
    # retranslateUi

