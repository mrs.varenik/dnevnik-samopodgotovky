import sys
import datetime
import inspect
import pathlib
import random
import re
from datetime import datetime
from docx.enum.text import WD_ALIGN_PARAGRAPH
from test import Sheduler
from docx import Document
from PySide2 import QtCore, QtGui, QtWidgets
from main_form import Ui_Dialog
from exit_form import Ui_Dialog_1
from welcome_form import Ui_Dialog_2
from error_form import Ui_Dialog_3
from error_input_form import Ui_Dialog_4
from save_form import Ui_Dialog_5

# начало создания приложения
app = QtWidgets.QApplication(sys.argv)
# глобальные имена, которые использовались практически в каждой процедурк
global name
global group
global fileName
# создали объект документа - пустую страницу ворда

# процедуры, если ввод некорректный
def error():
    Dialog_3.show()
    ui_3.pushButton_4.clicked.connect(bp_exit) #закрыть все окна

# процедура ошибки, если заполнены не все поля
def error_input():
    Dialog_4.show()
    ui_4.pushButton_4.clicked.connect(bp_exit) #закрыть все окна

#процедура получения пути к файлу, его имени и сохранение файла
def get_file_name():
    global fileName
    global name
    global group
    document_name = "Дневник самоподготовки - " + name + " " + group
    fileName = QtWidgets.QFileDialog.getSaveFileName ( None,'Open file', 'E:' + document_name,"Document files (*.docx)") #получение пути и имени файла
    ui_5.label.setText(fileName[0]) #в необходимом поле высвечивается путь к файлу

# открытие снова ока ввода данных
def main_form_again():
    Dialog_5.close()
    Dialog.show()
    ui.pushButton.clicked.connect(bp)

# появление формы сохранения
def save_form():
    global fileName
    if (fileName == []): get_file_name() # создано, чтобы было возможно повторное создание документа
    ui_5.pushButton_2.clicked.connect(exit_form) #переход на последнее окно

#процедура, закрывающая все окна
def bp_exit():
    Dialog_1.close()
    Dialog_3.close()
    Dialog_4.close()

def exit_form():
    Dialog_5.close()
    Dialog.close()
    Dialog_1.show() #открывается последнее окно, остальные закрываются
    global fileName
    global document
    document.save(fileName[0]) #сохранение файла по полученнному от пользователя пути
    ui_2.label_11.setText(str(fileName[0])) # вывод поути
    ui_2.pushButton_3.clicked.connect(bp_exit) # выоход из программы
    ui_2.pushButton_4.clicked.connect(bp_start) # создание дополнительного дневника

def bp():
    global name
    global group
    global document
#считывание введенных пользователем данных
    name = ui.lineEdit.text()
    date_of_birth = ui.dateEdit.text()
    group = ui.lineEdit_3.text()
    weight = ui.lineEdit_4.text()
    height = ui.lineEdit_5.text()
    date_1 = ui.dateEdit_2.date()
    date_2 = ui.dateEdit_3.date()
    #преобразование периоды в нужный формат
    date_1 =  date_1.toString("dd.MM.yyyy")
    date_2 =  date_2.toString("dd.MM.yyyy")
    #опредление группы здоровья
    group_health = "OFP" if ui.comboBox.currentIndex() == 0 else "FLEX"
#проверка на корректность ввода
    key_1 = re.search('\d+', name) is not None # проверка на корректность имени
    group = group.replace(" ", "")
    n = len(group)
    l = 1
    if (group[n-1] == "Б") or (group[n-1] == "б"):
        l+=1
    key = False
    if (group[n-l]).isdigit():
        l+=1
        if (group[n-l]).isdigit():
            l+=1
            if (group[n-l] == "-"):
                l+=1
                if (group[n-l]).isdigit():
                    key = True
    if ((name == "") or (group == "") or (weight == "") or (height == "")):
        error() # вызов соответсвующей процедуры ошики
    elif ((height.isdigit() == False) or (weight.isdigit() == False) or (height.isdigit() <= 0) or (weight.isdigit() <=0) or (key_1 == True) or (key == False)):
        error_input() # вызов ошибки некорретного ввода
    else:
        global document
        document = Document()
        document.add_heading("Дневник самоподготовки", 0)
        # добавляем таблицу в документ с одной строкой и двумя колонками
        # при этом получаем объект этой таблица - table
        # дальше сможем ее наполнять, работая с этим объектом
        table = document.add_table(rows=1, cols=2)
        # добавили новую строку в таблице - add_row
        # и полулчили массви (list) ее ячеек - cells
        # аналогично, сможем наполнять каждую ячеку этой строки через элемента массива
        row = table.add_row().cells
        row[0].text = "ФИО"
        row[1].text = name
        row = table.add_row().cells
        row[0].text = "Дата рождения"
        row[1].text = date_of_birth

        row = table.add_row().cells
        row[0].text = "Группа"
        row[1].text = group

        row = table.add_row().cells
        row[0].text = "Вес"
        row[1].text = weight

        row = table.add_row().cells
        row[0].text = "Рост"
        row[1].text = height

        # объявили названия дней на русском языке
        # библиотека может выдать только их английские названия или номера, считая от 0
        weekdays = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
        day_time = ["утро", "день"]

        # еще одна таблица, уже на 5 колонок
        table = document.add_table(rows=1, cols=5)
        table.style = "Table Grid"

        # в самую первую (с номером 0) строку записали названия колонок и выравнили по центру
        hdr_cells = table.rows[0].cells
        p = hdr_cells[0].add_paragraph("День недели, число, месяц, год, время занятия")
        p.alignment=WD_ALIGN_PARAGRAPH.CENTER
        p = hdr_cells[1].add_paragraph("Содержания физкультурного занятия")
        p.alignment=WD_ALIGN_PARAGRAPH.CENTER
        p = hdr_cells[2].add_paragraph("Пульс")
        p.alignment=WD_ALIGN_PARAGRAPH.CENTER
        p = hdr_cells[3].add_paragraph("Самочувствие")
        p.alignment=WD_ALIGN_PARAGRAPH.CENTER
        p = hdr_cells[4].add_paragraph("Желание заниматься")
        p.alignment=WD_ALIGN_PARAGRAPH.CENTER

        # получили объект создателя расписания занятий sheduler (глагол от слова "расписание")
        she = Sheduler(1,[2,5],1,[group_health],4,date_1, date_2,69,135)

        # в поле shed объекта she лежит какой-то перечисляемый тип. Мы перебираем всего его значения
        # в цилке for, каждый раз принимая новый элемент за переменную day
        for day in she.shed:
            # day - это словарь (dict), к его полям обращаемся по именам
            # в day['date'] лежит дата. Это объект класса datetime, мы вызываем его метод
            # strftime чтобы получить дату в нужном виде, согласно форме.

            # у модуля random есть функция choice, она выбирает случайное значение из перчесляемого объекта
            # который ей передали. Тут мы передали day_time и она случайно выберет из него, утро или день

            # join соединяет массив строк в строку через разделитель, в данном случае, через \n, то есть
            # через сброс строки (погугли join, у него странный внешний вид, но работает логично)

            # получается, мы имеем в string_date дату занятия и либо утро, либо день
            string_date = "\n".join([day['date'].strftime("%d.%m.%Y"), random.choice(day_time)])
            # f"" - это эф-стринг, строка, в котору можно подставлять любые значения, переменные, она сделает их своей частью
            # тут мы создаем массив description (описание), для каждого упражнения (ex) в поле 'ex' словаря day
            # мы создаем строку, в которой через знак сброса строки записываем ex['title'] - название упражнения
            # и ex['value'] - количество повторений. Судя по манере обращения к ex, это тоже словарь
            # по тоге получаем массив, вроде такого:
            # ["отжимания\n2 подхода по 10 раз", "подтягивания\n3 подхода по 3 раза"]

            description = [f"{ex['title']}\n{ex['value']}" for ex in day['ex']]
            # добавляем новюу строку в таблицу и начинаем заполнять
            row_cells = table.add_row().cells
            # внесли дату
            p = row_cells[0].add_paragraph(string_date)
            p.alignment=WD_ALIGN_PARAGRAPH.CENTER
            # соединили все упражнения с их повторениями вместе через два сброса строки
            # они одно под другим, и между ними еще пропуск строки, как два раза подряд на enter нажать
            row_cells[1].text = "\n\n".join(description)
            # вносим в остальные поля таблицы данные из day, пуль, состояине и настроение
            row_cells[2].text = day['pulse']
            p = row_cells[3].add_paragraph(day['state'])
            p.alignment=WD_ALIGN_PARAGRAPH.CENTER
            p = row_cells[4].add_paragraph(day['mood'])
            p.alignment=WD_ALIGN_PARAGRAPH.CENTER

            Dialog.close()
            Dialog_5.show()
            ui_5.pushButton_3.clicked.connect(main_form_again)

            ui_5.pushButton.clicked.connect(save_form) # переход на форму сохранения при нажатии на кнопку
# старотовая функция, которая очищает все поля и даты в момент запуска программы
# необходима, если пользователь решит создать еще один дневник

#функция, открывающая заново стартовое окно
def start_again():
    Dialog.close()
    Dialog_2.show()
    ui_1.pushButton_2.clicked.connect(bp_start)

#закрытие всех старых окон
def bp_start():
    Dialog_1.close()
    Dialog_2.close()
    # откртие главного
    Dialog.show()
    #очистка полей
    ui.lineEdit.setText("")
    ui.lineEdit_3.setText("")
    ui.lineEdit_4.setText("")
    ui.lineEdit_5.setText("")
    ui_5.label.setText("")
    ui_2.label_11.setText("")
    global fileName
    #необходимо для создания второго файла
    fileName = []

    # дата рождения и период введения дневника - по умолчанию.
    #крайний период - сегодняшний день
    ui.dateEdit.setDate(QtCore.QDate(1999, 1, 1))
    ui.dateEdit_2.setDate(QtCore.QDate(2020, 11, 20))
    ui.dateEdit_3.setDateTime(QtCore.QDateTime.currentDateTime())
    ui.pushButton.clicked.connect(bp) #переход на окно сохранения введеных данных
    ui.pushButton_2.clicked.connect(start_again)

# создание стартового окна и его атрибутов
Dialog_2 = QtWidgets.QDialog()
ui_1 = Ui_Dialog_2()
ui_1.setupUi(Dialog_2)
Dialog_2.show()

# создание главного окна ввода и подключене к его атрибутам
Dialog = QtWidgets.QDialog()
ui = Ui_Dialog()
ui.setupUi(Dialog)

# создание окна выхода и подключене к его атрибутам
Dialog_1 = QtWidgets.QDialog()
ui_2 = Ui_Dialog_1()
ui_2.setupUi(Dialog_1)

# создание окна вывода ошибки пустых строк и подключене к его атрибутам
Dialog_3 = QtWidgets.QDialog()
ui_3 = Ui_Dialog_3()
ui_3.setupUi(Dialog_3)

# создание окна вывода ошибки некорректного ввода и подключене к его атрибутам
Dialog_4 = QtWidgets.QDialog()
ui_4 = Ui_Dialog_4()
ui_4.setupUi(Dialog_4)

# создание окна сохранения и подключене к его атрибутам
Dialog_5 = QtWidgets.QDialog()
ui_5 = Ui_Dialog_5()
ui_5.setupUi(Dialog_5)
ui_1.pushButton_2.clicked.connect(bp_start)

sys.exit(app.exec_())
