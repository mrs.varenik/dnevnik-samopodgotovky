# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_error.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog_3(object):
    def setupUi(self, Dialog_3):
        if not Dialog_3.objectName():
            Dialog_3.setObjectName(u"Dialog_3")
        Dialog_3.resize(500, 200)
        Dialog_3.setMinimumSize(QSize(500, 200))
        Dialog_3.setMaximumSize(QSize(500, 200))
        icon = QIcon()
        icon.addFile(u"../p7/myappico.ico", QSize(), QIcon.Normal, QIcon.Off)
        Dialog_3.setWindowIcon(icon)
        Dialog_3.setStyleSheet(u"background: qlineargradient(spread:pad, x1:0.865, y1:0.818182, x2:1, y2:1, stop:0 rgba(38, 66, 54, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label = QLabel(Dialog_3)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(90, 40, 321, 51))
        self.label.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 15pt \"Times New Roman\";\n"
"background: none;")
        self.pushButton_4 = QPushButton(Dialog_3)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setGeometry(QRect(190, 130, 121, 31))
        self.pushButton_4.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"")

        self.retranslateUi(Dialog_3)

        QMetaObject.connectSlotsByName(Dialog_3)
    # setupUi

    def retranslateUi(self, Dialog_3):
        Dialog_3.setWindowTitle(QCoreApplication.translate("Dialog_3", u"\u041e\u0448\u0438\u0431\u043a\u0430", None))
        self.label.setText(QCoreApplication.translate("Dialog_3", u"\u041d\u0435 \u0432\u0441\u0435 \u043f\u043e\u043b\u044f \u0437\u0430\u043f\u043e\u043b\u043d\u0435\u043d\u044b", None))
        self.pushButton_4.setText(QCoreApplication.translate("Dialog_3", u"\u041e\u043a", None))
    # retranslateUi

