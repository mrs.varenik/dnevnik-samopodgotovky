# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_error_input.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog_4(object):
    def setupUi(self, Dialog_4):
        if not Dialog_4.objectName():
            Dialog_4.setObjectName(u"Dialog_4")
        Dialog_4.resize(500, 200)
        Dialog_4.setMinimumSize(QSize(500, 200))
        Dialog_4.setMaximumSize(QSize(500, 200))
        icon = QIcon()
        icon.addFile(u"../p7/myappico.ico", QSize(), QIcon.Normal, QIcon.Off)
        Dialog_4.setWindowIcon(icon)
        Dialog_4.setStyleSheet(u"background: qlineargradient(spread:pad, x1:0.865, y1:0.818182, x2:1, y2:1, stop:0 rgba(38, 66, 54, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label = QLabel(Dialog_4)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(50, 20, 391, 101))
        self.label.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 13pt \"Times New Roman\";\n"
"background: none;")
        self.pushButton_4 = QPushButton(Dialog_4)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setGeometry(QRect(190, 130, 121, 31))
        self.pushButton_4.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"")

        self.retranslateUi(Dialog_4)

        QMetaObject.connectSlotsByName(Dialog_4)
    # setupUi

    def retranslateUi(self, Dialog_4):
        Dialog_4.setWindowTitle(QCoreApplication.translate("Dialog_4", u"\u041e\u0448\u0438\u0431\u043a\u0430", None))
        self.label.setText(QCoreApplication.translate("Dialog_4", u"<html><head/><body><p align=\"center\">\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u043e \u0432\u0432\u0435\u0434\u0435\u043d\u044b \u0434\u0430\u043d\u043d\u044b\u0435</p><p align=\"center\">\u041f\u043e\u0432\u0442\u043e\u0440\u0438\u0442\u0435 \u0432\u0432\u043e\u0434</p></body></html>", None))
        self.pushButton_4.setText(QCoreApplication.translate("Dialog_4", u"\u041e\u043a", None))
    # retranslateUi

