# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_main.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(900, 550)
        Dialog.setMinimumSize(QSize(900, 550))
        Dialog.setMaximumSize(QSize(650, 400))
        font = QFont()
        font.setFamily(u"Times New Roman")
        font.setPointSize(10)
        Dialog.setFont(font)
        icon = QIcon()
        icon.addFile(u"myappico.ico", QSize(), QIcon.Normal, QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setAutoFillBackground(False)
        Dialog.setStyleSheet(u"background: url('E:/p4/qqq.jpg');\n"
"\n"
"\n"
"background-color: qlineargradient(spread:pad, x1:0.0607735, y1:0.159091, x2:1, y2:1, stop:0.707182 rgba(170, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.lineEdit = QLineEdit(Dialog)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setGeometry(QRect(240, 100, 511, 40))
        self.lineEdit.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 10pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.lineEdit_3 = QLineEdit(Dialog)
        self.lineEdit_3.setObjectName(u"lineEdit_3")
        self.lineEdit_3.setGeometry(QRect(190, 300, 181, 41))
        self.lineEdit_3.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 10pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;")
        self.lineEdit_4 = QLineEdit(Dialog)
        self.lineEdit_4.setObjectName(u"lineEdit_4")
        self.lineEdit_4.setGeometry(QRect(660, 200, 181, 41))
        self.lineEdit_4.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 10pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(30, 80, 401, 81))
        self.label.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 14pt \"Times New Roman\";\n"
"background: none;")
        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(30, 210, 191, 81))
        self.label_2.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 14pt \"Times New Roman\";\n"
"background: none;")
        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(30, 300, 131, 31))
        self.label_3.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 14pt \"Times New Roman\";\n"
"background: none;")
        self.label_4 = QLabel(Dialog)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(510, 210, 101, 31))
        self.label_4.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 14pt \"Times New Roman\";\n"
"background: none;")
        self.label_5 = QLabel(Dialog)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(510, 290, 101, 21))
        self.label_5.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 14pt \"Times New Roman\";\n"
"background: none;")
        self.label_6 = QLabel(Dialog)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(350, 20, 251, 41))
        self.label_6.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 18pt \"Times New Roman\";\n"
"background: none;")
        self.label_7 = QLabel(Dialog)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(-20, -10, 1100, 700))
        self.label_7.setMinimumSize(QSize(1100, 700))
        self.label_7.setMaximumSize(QSize(60, 90))
        self.label_7.setStyleSheet(u"background: rgba(0, 0, 0, 0.3)")
        self.lineEdit_5 = QLineEdit(Dialog)
        self.lineEdit_5.setObjectName(u"lineEdit_5")
        self.lineEdit_5.setGeometry(QRect(660, 270, 181, 41))
        self.lineEdit_5.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 10pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;")
        self.dateEdit = QDateEdit(Dialog)
        self.dateEdit.setObjectName(u"dateEdit")
        self.dateEdit.setGeometry(QRect(190, 230, 181, 41))
        self.dateEdit.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 11pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"background-color: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"selection-background-color: qlineargradient(spread:pad, x1:0.865, y1:0.818182, x2:1, y2:1, stop:0 rgba(38, 66, 54, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.comboBox = QComboBox(Dialog)
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setGeometry(QRect(190, 370, 181, 41))
        self.comboBox.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 11pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"background-color: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"selection-background-color: qlineargradient(spread:pad, x1:0.865, y1:0.818182, x2:1, y2:1, stop:0 rgba(38, 66, 54, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.label_8 = QLabel(Dialog)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(30, 350, 121, 71))
        self.label_8.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 14pt \"Times New Roman\";\n"
"background: none;")
        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(370, 450, 111, 31))
        self.pushButton.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 12pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"")
        self.dateEdit_2 = QDateEdit(Dialog)
        self.dateEdit_2.setObjectName(u"dateEdit_2")
        self.dateEdit_2.setGeometry(QRect(660, 340, 181, 41))
        self.dateEdit_2.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 11pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"background-color: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"selection-background-color: qlineargradient(spread:pad, x1:0.865, y1:0.818182, x2:1, y2:1, stop:0 rgba(38, 66, 54, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.dateEdit_3 = QDateEdit(Dialog)
        self.dateEdit_3.setObjectName(u"dateEdit_3")
        self.dateEdit_3.setGeometry(QRect(660, 410, 181, 41))
        self.dateEdit_3.setStyleSheet(u"background-image: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 11pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"background-color: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"selection-background-color: qlineargradient(spread:pad, x1:0.865, y1:0.818182, x2:1, y2:1, stop:0 rgba(38, 66, 54, 255), stop:1 rgba(255, 255, 255, 255));\n"
"")
        self.label_11 = QLabel(Dialog)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setGeometry(QRect(510, 320, 131, 61))
        self.label_11.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 12pt \"Times New Roman\";\n"
"background: none;\n"
"")
        self.label_9 = QLabel(Dialog)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setGeometry(QRect(510, 380, 151, 71))
        self.label_9.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"font: 12pt \"Times New Roman\";\n"
"background: none;\n"
"")
        self.pushButton_2 = QPushButton(Dialog)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(30, 450, 41, 31))
        self.pushButton_2.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background: qlineargradient(spread:pad, x1:0.713483, y1:0.773, x2:0.960674, y2:1, stop:0 rgba(21, 43, 31, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 15pt \"Times New Roman\";\n"
"border: 1px solid #ffffff;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.label_7.raise_()
        self.lineEdit.raise_()
        self.lineEdit_3.raise_()
        self.lineEdit_4.raise_()
        self.label.raise_()
        self.label_2.raise_()
        self.label_3.raise_()
        self.label_4.raise_()
        self.label_5.raise_()
        self.label_6.raise_()
        self.lineEdit_5.raise_()
        self.dateEdit.raise_()
        self.comboBox.raise_()
        self.label_8.raise_()
        self.pushButton.raise_()
        self.dateEdit_2.raise_()
        self.dateEdit_3.raise_()
        self.label_11.raise_()
        self.label_9.raise_()
        self.pushButton_2.raise_()

        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"\u0414\u043d\u0435\u0432\u043d\u0438\u043a \u0441\u0430\u043c\u043e\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438", None))
        self.lineEdit.setText("")
        self.lineEdit_3.setText("")
        self.lineEdit_4.setText("")
        self.label.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p>\u0424\u0430\u043c\u0438\u043b\u0438\u044f \u0418\u043c\u044f</p><p>\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e</p></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p>\u0414\u0430\u0442\u0430</p><p>\u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f</p></body></html>", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"\u0413\u0440\u0443\u043f\u043f\u0430", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"\u0412\u0435\u0441", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"\u0420\u043e\u0441\u0442", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", None))
        self.label_7.setText("")
        self.lineEdit_5.setText("")
        self.comboBox.setItemText(0, QCoreApplication.translate("Dialog", u"\u041e\u0424\u041f", None))
        self.comboBox.setItemText(1, QCoreApplication.translate("Dialog", u"\u0421\u043f\u0435\u0446\u043c\u0435\u0434", None))

        self.label_8.setText(QCoreApplication.translate("Dialog", u"\u0413\u0440\u0443\u043f\u043f\u0430\n"
"\u0437\u0434\u043e\u0440\u043e\u0432\u044c\u044f", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"\u0421\u043e\u0437\u0434\u0430\u0442\u044c", None))
        self.label_11.setText(QCoreApplication.translate("Dialog", u"\u0414\u0430\u0442\u0430\n"
"\u043d\u0430\u0447\u0430\u043b\u0430", None))
        self.label_9.setText(QCoreApplication.translate("Dialog", u"\u0414\u0430\u0442\u0430\n"
"\u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"\u2190", None))
    # retranslateUi
